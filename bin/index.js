#!/usr/bin/env node
const chalk = require("chalk")
const Spinner = require('cli-spinner').Spinner;
const mongoose = require('mongoose');
var Task = require('./Models/task.model');


const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

var spinner = new Spinner();
spinner.setSpinnerString('|/-\\');


welcome = async () => {
    await console.clear()
    await console.log(chalk.blue(chalk.bold("Welcome to Prahem Tasks")))
}




createTask = async () => {
    var task = new Task({
        name: null,
        description: null,
        status: "Is Processing"
    });
    readline.question(chalk.green(`Enter the task name: \n`), async (name) => {
        if (name) {
            task.name = await name;
            readline.question(chalk.green(`Enter the task description: \n`), async (des) => {
                task.description = await des;
                await spinner.start()

                task.save(function (err) {
                    if (err) throw err;
                    console.log('Task saved successfully!');
                });
                console.log(chalk.italic(`\n ${task.name} created`))
                await spinner.stop()
                await viewOptions()
            })
        } else {
            console.log(chalk.red("Name not valid"))
            await createTask()
        }
    })
}
viewOptions = async () => {
    await readline.question(chalk.blue(chalk.bold(`\nSelect an option : \n1. Create Task\n2. List all Tasks\n3. Edit by Task name\n\n\n`)), async (reply) => {
        switch (reply) {
            case "1":
                await createTask()
                await viewOptions()
                break;
            case "2":
                await getAllTasks()
                await viewOptions()
                break;
            case "3":
                await EditByTaskName()
                await viewOptions()
                break;
            default:
                break;
        }
    })
}


connectToDB = async () => {
    await spinner.start()
    await mongoose.connect("mongodb+srv://arqam:super123@prahem-zwkv7.mongodb.net/test?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, db) {
        if (err) throw err;
    });
    console.log(chalk.blue("Connected to Database"))
    await spinner.stop()
}
getAllTasks = async () => {
    let allTasks = await Task.find();
    allTasks.forEach((snap, idx) => {
        console.log(`${idx + 1}) ${snap.name}\n Description : ${chalk.italic(snap.description)}\n Status : ${chalk.italic(snap.status)}\n\n`)
    })
}

EditByTaskName = async () => {
    await readline.question(chalk.green(`Enter the task name to edit: \n`), async (taskname) => {
        const searchedTask = await Task.find({
            $or: [{ "name": { "$regex": taskname, '$options': 'i' } }],
        }).limit(10)
        if (searchedTask.length) {
            searchedTask.forEach((snap, idx) => {
                console.log(idx + 1, snap.name)
            })
            await readline.question(chalk.green(`Enter the number of the task to edit: \n`), async (tasknum) => {
                if (tasknum) {
                    readline.question(chalk.green(`\n1. Mark is as processed\n2. Edit the description\n`), async (editOption) => {
                        console.log(chalk.bold(editOption))
                        switch (editOption) {
                            case "1":
                                if (searchedTask[tasknum - 1].status == "Processed") {
                                    console.log(chalk.red("Task was already processed"))
                                    await viewOptions()
                                } else {
                                    searchedTask[tasknum - 1].status = "Processed"
                                    await updateTask(searchedTask[tasknum - 1], searchedTask[tasknum - 1]._id)
                                    await console.log(chalk.greenBright("Task Processed"))
                                }
                                break;
                            case "2":
                                readline.question(chalk.green(`Enter new description\n`), async (description) => {
                                    searchedTask[tasknum - 1].description = description
                                    updateTask(searchedTask[tasknum - 1], searchedTask[tasknum - 1]._id)
                                    await console.log(chalk.greenBright("Description updated"))
                                })
                                break;
                            default: console.log(chalk.red("Wrong Option"))
                                break;
                        }
                    })
                } else {
                    console.log(chalk.red("Task Number not selected"))
                }
            })
        } else {
            console.log(chalk.red("No Tasks found"))
            EditByTaskName();
        }
    })
}

updateTask = async (data, id) => {
    await Task.updateOne({ _id: id }, data)
    await viewOptions()
}
start = async () => {
    await welcome();
    await connectToDB();
    await viewOptions();
}

start();




